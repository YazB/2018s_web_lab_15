<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="ictgradschool.web.lab15.ex1.LoggingTable" %>
<%@ page import="ictgradschool.web.lab15.ex1.AccessLog" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Exercise 01</title>
    </head>
    <body>
        Generate a well formed table of AccessLogs here using JSTL/EL.
        <%--//form above table--%>
        <form action =" " method="post">
            Name<br>
            <input type="text" name="name" value="${param.name}">
            <br>
            Description<br>
            <input type="text" name="description" value="${param.description}">
            <br>
            <input type ="submit">
        </form>
        <table style="width:100%">

            <thead>
                <tr>
                    <th align="left">ID</th>
                    <th align="left">Name</th>
                    <th align="left">Description</th>
                    <th align="left">Timestamp</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <c:forEach items="${finds}" var="item">
                            ${item.id}<br>
                        </c:forEach>
                            <c:forEach items="${element}" var="myCollection" varStatus="loopStatus">
                <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                    ...
                </tr>
                </c:forEach>
                    </td>
                    <td>
                        <c:forEach items="${finds}" var="item">
                            ${item.name}<br>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach items="${finds}" var="item">
                            ${item.desc}<br>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach items="${finds}" var="item">
                            ${item.timestamp}<br>
                        </c:forEach>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <%--Here I am trying to do if condition for foot--%>
                <c:set var = "size" scope = "session" value = "${size.rowCount}"/>
                <c:if test = "${size.rowCount > 30}">
                    <tr>
                        <th align="left">ID</th>
                        <th align="left">Name</th>
                        <th align="left">Description</th>
                        <th align="left">Timestamp</th>
                    </tr>
                </c:if>
            </tfoot>
        </table>
    </body>
</html>
