package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class LoggingTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Retrieve LoggingTable entries and pass them to the LoggingTableDisplay.jsp file
        AccessLogDAO dao= new AccessLogDAO();
        List<AccessLog> find = dao.allAccessLogs();
        request.setAttribute("finds", find);
        request.getRequestDispatcher("LoggingTableDisplay.jsp").forward(request, response);
    }
}
