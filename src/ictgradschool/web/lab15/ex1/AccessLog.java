package ictgradschool.web.lab15.ex1;

import java.io.Serializable;



public class AccessLog implements Serializable {

    public String accessLog;
    public int id;
    public String name;
    public String desc;
    public String timestamp;

    public AccessLog() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public AccessLog(int id, String name, String desc, String timestamp) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.timestamp = timestamp;

    }

}
