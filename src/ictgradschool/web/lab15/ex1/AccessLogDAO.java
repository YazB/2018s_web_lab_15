package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletContext;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;


public class AccessLogDAO {
    Properties dbProps;

    public AccessLogDAO(ServletContext context) {
        dbProps = new Properties();
        //intializing dbProps to store passwords

//Was using wrong driver before!
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/db.properties"))) {
            dbProps.load(fIn);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List <AccessLog> allAccessLogs() {

        List <AccessLog> logs = new ArrayList <>();

//        dbProperties.setProperty("url", "jdbc:mariadb://db.sporadic.nz:3306/yab2");
//        dbProperties.setProperty("user", "yab2");
//        dbProperties.setProperty("password", "KindlyPreciousWayUnwitting");

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("SUCCESS");
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet rs = stmt.executeQuery("SELECT * FROM access_log;")) {
                    while (rs.next()) {
                        AccessLog a = new AccessLog();
                        a.setId(rs.getInt(1));
                        a.setName(rs.getString(2));
                        a.setDesc(rs.getString(3));
                        a.setTimestamp(rs.getTimestamp(4).toString());
                        logs.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return logs;
    }

    public void addAcessLog(AccessLog a) {

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
//            System.out.println("Success");
            try (PreparedStatement s = conn.prepareStatement("INSERT INTO access_log (name, desc) VALUES (?, ?)")) {
                s.setString(1, a.getName());
                s.setString(2, a.getDesc());

                s.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
